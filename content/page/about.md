---
title: About me
subtitle: Some of the highlights
comments: false
---

Hi, I'm James. 

I test software for a living and am interested in finding systems that help make life better.  

Some other stuff about me:

- I enjoy learning new things, especially about technology
- I like figuring out how to break things
- I love ballroom dancing in my free time!

### Educational Background
- Bachelor of Science in Electrical Engineering from Rose-Hulman Institute of Technology in 2009
- Master of Electrical & Computer Engineering from Rose-Hulman Institute of Technology in 2010
- Master of Business Administration from Indiana University in 2017

### Some of My Favorite Systems
- [Fisher Space Pen](http://www.spacepen.com/) with clip to fit inside of a wallet.  I fit four of these into my wallet, with four different colors (Black, Blue, Red, and Purple) so that I always have a pen handy for taking notes, etc...I get some looks when I pull out my wallet sometimes, but it's definitely a practical way to carry pens when you don't carry a bag around.
